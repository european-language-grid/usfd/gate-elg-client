/*
 *    Copyright 2019 The University of Sheffield
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.client;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

/**
 * Reference counting static manager for the underlying Vert.X WebClient so multiple duplicates or multiple instances of
 * this PR in the same app can share the same underlying netty thread pool and event loop.
 */
public class ClientManager {

  private static volatile long refCount = 0;

  private static Vertx vertx = null;

  private static WebClient webClient = null;

  public static synchronized WebClient claimClient() {
    refCount++;
    if(webClient == null) {
      vertx = Vertx.vertx();
      webClient = WebClient.create(vertx, new WebClientOptions().setUserAgent("GateElgClient/0.1"));
    }
    return webClient;
  }

  public static synchronized void releaseClient() {
    refCount--;
    if(refCount == 0) {
      webClient.close();
      webClient = null;
      vertx.close();
      vertx = null;
    }
  }
}
