/*
 *    Copyright 2019 The University of Sheffield
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.client;

import eu.elg.model.*;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;
import eu.elg.model.responses.ClassificationResponse;
import gate.AnnotationSet;
import gate.FeatureMap;
import gate.Resource;
import gate.Utils;
import gate.corpora.RepositioningInfo;
import gate.creole.AbstractLanguageAnalyser;
import gate.creole.ExecutionException;
import gate.creole.ExecutionInterruptedException;
import gate.creole.metadata.CreoleParameter;
import gate.creole.metadata.CreoleResource;
import gate.creole.metadata.Optional;
import gate.creole.metadata.RunTime;
import gate.util.InvalidOffsetException;
import io.vertx.ext.web.client.WebClient;
import org.apache.log4j.Logger;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * GATE PR that can call the "internal" ELG API for IE services the expect a "text" request and return an "annotations"
 * response.  Passes the GATE document content as the request content and maps the response annotations back to GATE
 * annotations.
 */
@CreoleResource(name = "ELG Client", comment = "Call an ELG IE service")
public class ElgClientPR extends AbstractLanguageAnalyser {

  /**
   * Used to resolve ELG-standard message codes.
   */
  private ResourceBundle messagesRB = ResourceBundle.getBundle("eu.elg.elg-messages");

  private static final Logger log = Logger.getLogger(ElgClientPR.class);

  private WebClient webClient;

  private URL serviceUrl;

  private String outputASName;

  private FeatureMap requestParams;

  private FeatureMap annotationTypeMapping;

  public URL getServiceUrl() {
    return serviceUrl;
  }

  @RunTime
  @CreoleParameter(comment = "URL of the endpoint to call")
  public void setServiceUrl(URL serviceUrl) {
    this.serviceUrl = serviceUrl;
  }

  public String getOutputASName() {
    return outputASName;
  }

  @Optional
  @RunTime
  @CreoleParameter(comment = "Annotation set in which to place the result annotations that do not have an explicit " +
          "set provided by annotationTypeMappings")
  public void setOutputASName(String outputASName) {
    this.outputASName = outputASName;
  }

  public FeatureMap getRequestParams() {
    return requestParams;
  }

  @Optional
  @RunTime
  @CreoleParameter(comment = "Additional \"params\" to add to each ELG request")
  public void setRequestParams(FeatureMap requestParams) {
    this.requestParams = requestParams;
  }

  public FeatureMap getAnnotationTypeMapping() {
    return annotationTypeMapping;
  }

  @Optional
  @RunTime
  @CreoleParameter(comment = "Mapping from the annotation types returned by the ELG service to the annotation types " +
          "(and optionally sets) in which GATE annotations should be created.  Keys in this map should be annotation " +
          "types as returned by the ELG endpoint, and the values should be either plain annotation types (which will " +
          "be placed in the outputASName set) or \"selector\" expressions of the form annotationSetName:annotationType. " +
          "For example, a mapping with the key \"/entities/people\" to the value \"NE:Person\" would map every " +
          "\"/entities/people\" annotation in the ELG response to a \"Person\" annotation in the \"NE\" annotation " +
          "set, ignoring the outputASName parameter.")
  public void setAnnotationTypeMapping(FeatureMap annotationTypeMapping) {
    this.annotationTypeMapping = annotationTypeMapping;
  }

  public Resource init() {
    webClient = ClientManager.claimClient();
    return this;
  }

  public void cleanup() {
    ClientManager.releaseClient();
    webClient = null;
  }


  @SuppressWarnings("unchecked")
  public void execute() throws ExecutionException {
    String content = getDocument().getContent().toString();

    TextRequest request = new TextRequest().withContent(content)
            .withMimeType("text/plain").withMarkup(
                    new Markup().withFeatures((Map) getDocument().getFeatures()));
    if(requestParams != null) {
      request.setParams((Map) requestParams);
    }

    CompletableFuture<ResponseMessage> respF = new CompletableFuture<>();
    webClient.postAbs(serviceUrl.toExternalForm()).putHeader("Accept", "application/json").sendJson(request, (ar) -> {
      if(ar.succeeded()) {
        try {
          respF.complete(ar.result().bodyAsJson(ResponseMessage.class));
        } catch(Throwable e) {
          // couldn't parse response for whatever reason
          respF.completeExceptionally(e);
        }
      } else {
        respF.completeExceptionally(ar.cause());
      }
    });

    try {
      ResponseMessage responseMessage = respF.get();

      if(responseMessage.getFailure() != null) {
        Failure fail = responseMessage.getFailure();
        String formattedErrors = fail.getErrors().stream().map(msg ->
          MessageFormat.format(messagesRB.containsKey(msg.getCode()) ? messagesRB.getString(msg.getCode()) : msg.getText(),
                  msg.getParams() == null ? new Object[0] : msg.getParams().toArray())).collect(Collectors.joining("\n"));
        throw new ExecutionException("Service call failed: " + formattedErrors);
      }
      Response<?> response = responseMessage.getResponse();
      if(response == null) {
        throw new ExecutionException("Service returned empty response");
      }
      if(response instanceof ClassificationResponse) {
        // map classification response to annotations response with features only
        response = new AnnotationsResponse().withFeature("elg-classes",
                ((ClassificationResponse) response).getClasses().stream().map(
                                cls -> Utils.featureMap(
                                        "class", cls.getClassName(),
                                        "score", cls.getScore()))
                        .collect(Collectors.toList()));
      }
      if(!(response instanceof AnnotationsResponse)) {
        throw new ExecutionException("Service sent " + response.getClass().getSimpleName() + " but expected annotations or classification response");
      }

      Map<String, List<AnnotationObject>> responseAnns = ((AnnotationsResponse) response).getAnnotations();
      if(responseAnns == null) {
        responseAnns = Collections.emptyMap();
      }
      AnnotationSet outputAS = getDocument().getAnnotations(outputASName);
      RepositioningInfo inRepos = repositionForSupplementaries(content);
      for(Map.Entry<String, List<AnnotationObject>> entry : responseAnns.entrySet()) {
        String type = entry.getKey();
        AnnotationSet thisOutputAS = outputAS;
        if(annotationTypeMapping != null) {
          String selector = (String)annotationTypeMapping.get(type);
          if(selector != null) {
            String[] setAndType = selector.split(":", 2);
            type = setAndType[setAndType.length - 1];
            if(setAndType.length > 1) {
              thisOutputAS = getDocument().getAnnotations(setAndType[0]);
            }
          }
        }
        List<AnnotationObject> annsOfType = entry.getValue();
        for(AnnotationObject a : annsOfType) {
          try {
            thisOutputAS.add(inRepos.getExtractedPos(a.getStart().longValue()), inRepos.getExtractedPos(a.getEnd().longValue()),
                    type, Utils.toFeatureMap(a.getFeatures()));
          } catch(InvalidOffsetException e) {
            log.warn("Annotation outside document content - ignored", e);
          }
        }
      }

      // handle features
      Map<String, Object> responseFeatures = ((AnnotationsResponse) response).getFeatures();
      if(responseFeatures != null) {
        getDocument().getFeatures().putAll(responseFeatures);
      }
    } catch(InterruptedException e) {
      throw new ExecutionInterruptedException(e);
    } catch(java.util.concurrent.ExecutionException e) {
      throw new ExecutionException(e.getCause());
    }
  }

  private static final Pattern CHARS_TO_ESCAPE = Pattern.compile("[\\x{" +
          Integer.toHexString(Character.MIN_SUPPLEMENTARY_CODE_POINT) + "}-\\x{" +
          Integer.toHexString(Character.MAX_CODE_POINT) + "}]");


  /**
   * GATE deals with annotation offsets in the same way as standard Java string indices, as UTF-16 code units. This
   * method calculates the adjustments necessary to shift the annotation offsets into terms of Unicode code points if
   * the document contains supplementary characters such as Emoji.
   *
   * @param text the document text, which may contain supplementary characters.
   * @return GATE RepositioningInfo representing the adjustments
   */
  private RepositioningInfo repositionForSupplementaries(String text) {
    int origOffset = 0;
    int extractedOffset = 0;
    RepositioningInfo repos = new RepositioningInfo();
    Matcher mat = CHARS_TO_ESCAPE.matcher(text);
    while(mat.find()) {
      if(mat.start() != extractedOffset) {
        // repositioning record for the span from end of previous match to start of this one
        int nonMatchLen = mat.start() - extractedOffset;
        repos.addPositionInfo(origOffset, nonMatchLen, extractedOffset, nonMatchLen);
        origOffset += nonMatchLen;
        extractedOffset += nonMatchLen;
      }

      // the extracted length is the number of code units matched by the pattern
      // (should always be 2 for the two halves of the surrogate pair)
      int extractedLen = mat.end() - mat.start();

      // repositioning record covering this match
      repos.addPositionInfo(origOffset, 1, extractedOffset, extractedLen);
      origOffset += 1;
      extractedOffset += extractedLen;
    }
    int tailLen = text.length() - extractedOffset;
    // repositioning record covering everything after the last match - not strictly
    // required on GATE 8.6+ if tailLen is zero but won't do any harm in that case
    // and works around a bug in earlier versions
    repos.addPositionInfo(origOffset, tailLen + 1, extractedOffset, tailLen + 1);

    return repos;
  }

}
